const express = require("express");
const expressLayout = require("express-ejs-layouts");
const mongoose = require('mongoose');
const app = express();
app.use(expressLayout);
app.set('view engine','ejs');
app.use('/', require('./routes/index'));
// Express body parser
app.use(express.urlencoded({ extended: true }));

//db connect
const db = require('./config/keys').MongoURI;
//connect to mongoose
mongoose.connect(db,{useNewUrlParser:true})
    .then(() => console.log('MongoDB connected...'))
    .catch(err => console.log(err))
//users js in pages//
app.use('/users', require('./routes/users'))

const PORT = process.env.PORT || 5000;

app.listen(PORT, console.log(`Server started on port: ${PORT}`));